import json

from datetime import datetime
from flask import redirect, render_template, url_for
from PyServer import app
from PyServer.form import *
from PyServer.views import apiUrl
from Model.recipe import *
from util import dictify

from PyServer import mongodb
import bson.json_util
from bson.objectid import ObjectId
from bson.json_util import dumps

@app.errorhandler(404)
@app.errorhandler(403)
def forbidden(status):
    """Renders the home page."""
    return render_template(
        'errors.html',
        title=status,
        year=datetime.now().year,
    )