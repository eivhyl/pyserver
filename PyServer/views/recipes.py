from PyServer import app
from PyServer.form import RecipeForm, IngredientForm
from flask import redirect, render_template, request, url_for, jsonify, abort, session
from PyServer.views import apiUrl, webpackURL
from datetime import datetime
from Model.recipe import Recipe
import urllib
import util
import json
import pymongo


@app.route('/recipes/')
def showrecipes():
    """Renders the recipe page"""
    try:
        url = apiUrl + 'recipes/'
        data = urllib.request.urlopen(url, timeout=1)
        data = json.loads(data.read().decode('utf-8'))
        return render_template(
            'recipes.html',
            title='All Recipes',
            year=datetime.now().year,
            recipes=data,
            app=app
        )

    except urllib.error.URLError:
        return abort(404)


@app.route('/recipes/<id>/')
def showrecipe(id):
    """Renders the recipe page"""
    try:
        url = apiUrl + 'recipes/' + id
        data = urllib.request.urlopen(url, timeout=1)
        data = json.loads(data.read().decode('utf-8'))
        return render_template(
            'showrecipe.html',
            title='All Recipes',
            year=datetime.now().year,
            recipe=data,
        )

    except urllib.error.URLError:
        return abort(404)


@app.route('/recipes/add/', methods=['GET', 'POST'])
def addrecipe():
    """Renders the recipe submission page."""

    try:

        if request.method == 'POST':
            recipe = request.get_json()
            if recipe is not None:
                return jsonify(recipe)
            else:
                abort(400)
            # if form.validate():
            #     recipe_obj.name = form.name.data
            #     recipe_obj.ingredients = form.ingredients.data
            #     recipe_obj.image = form.image.data
            #     recipe_obj.directions = form.directions.data
            #     recipe_obj.tips = form.tips.data
            #
            #     # Converts object to dict and inserts it to db
            #     recipe_id = table.insert_one(util.dictify(recipe_obj)).inserted_id
            #     return redirect('/recipes/' + str(pymongo.ObjectId(recipe_id)))
        elif request.method == 'GET':
            return render_template(
                'addrecipe.html',
                title='Submit Recipe',
                year=datetime.now().year,
                apiUrl=apiUrl,
                webpackURL=webpackURL
            )
    except KeyError:
        abort(403)
