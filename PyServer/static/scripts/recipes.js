/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	//TODO: Add bootstrap thumbnail class
	var Pages = React.createClass({
		displayName: 'Pages',

		getInitialState: function getInitialState() {
			return { items: [] };
		},

		componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
			var nextItems = this.state.items;
			var diff = nextProps.pages - this.props.pages;

			if (diff > 0) for (var i = 0; i <= diff; i++) {
				nextItems.push({ btnState: '' });
			} else if (diff < 0) for (var _i = diff; _i < 0; _i++) {
				nextItems.pop();
			}nextItems[this.props.currentPage - 1].btnState = '';
			nextItems[nextProps.currentPage - 1].btnState = 'active';

			this.setState({
				items: nextItems
			});
		},

		render: function render() {
			var _this = this;
			return React.createElement(
				'ul',
				{ className: 'pagination' },
				React.createElement(
					'li',
					{ onClick: _this.props.onSwapPage.bind(null, _this.props.currentPage - 1) },
					React.createElement(
						'a',
						{ href: '#', 'aria-label': 'Previous' },
						React.createElement(
							'span',
							{ 'aria-hidden': 'true' },
							'«'
						)
					)
				),
				this.state.items.map(function (props, i) {
					return React.createElement(
						'li',
						{ onClick: _this.props.onSwapPage.bind(null, i + 1), className: props.btnState, key: i },
						React.createElement(
							'a',
							{ href: '#' },
							i + 1
						)
					);
				}),
				React.createElement(
					'li',
					{ onClick: _this.props.onSwapPage.bind(null, _this.props.currentPage + 1) },
					React.createElement(
						'a',
						{ href: '#', 'aria-label': 'Next' },
						React.createElement(
							'span',
							{ 'aria-hidden': 'true' },
							'»'
						)
					)
				)
			);
		}
	});
	var RecipeList = React.createClass({
		displayName: 'RecipeList',

		render: function render() {
			var createItem = function createItem(recipe) {
				var image;
				if (recipe.image) image = React.createElement('img', { src: recipe.image });else image = React.createElement('img', { src: '../static/images/404.png' });

				return React.createElement(
					'div',
					{ key: recipe._id, className: 'panel panel-box col-xs-1' },
					React.createElement('a', { href: recipe._id }),
					React.createElement(
						'div',
						{ className: 'panel-title' },
						image
					),
					React.createElement(
						'div',
						{ className: 'panel-body' },
						React.createElement(
							'h3',
							null,
							recipe.name
						)
					)
				);
			};
			return React.createElement(
				'div',
				{ className: 'row' },
				this.props.items.map(createItem)
			);
		}
	});
	var RecipeApp = React.createClass({
		displayName: 'RecipeApp',

		getInitialState: function getInitialState() {
			return { items: [], elemsPerPage: 10, pages: 1, currentPage: 1 };
		},
		loadPage: function loadPage(i) {
			var _this = this;
			$.getJSON('http://localhost:3000/recipes/search' + '?page=' + i + '&limit=' + this.state.elemsPerPage, function (data) {
				var nextItems = data['results'];
				_this.setState({
					items: nextItems,
					currentPage: i,
					pages: Math.ceil(data['totalResults'] / _this.state.elemsPerPage)
				});
			});
		},
		componentDidMount: function componentDidMount() {
			this.loadPage(this.state.currentPage);
		},
		swapPage: function swapPage(i) {
			// Only swap to legal pages
			if (i !== this.state.currentPage && i > 0 && i <= this.state.pages) {
				this.loadPage(i);
			}
		},

		render: function render() {
			return React.createElement(
				'div',
				null,
				React.createElement(RecipeList, { items: this.state.items }),
				React.createElement(Pages, { pages: this.state.pages, onSwapPage: this.swapPage, currentPage: this.state.currentPage })
			);
		}
	});

	ReactDOM.render(React.createElement(RecipeApp, null), document.getElementById('recipes'));

/***/ }
/******/ ]);