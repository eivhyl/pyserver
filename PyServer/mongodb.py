from pymongo import MongoClient
from decouple import config

if not config('DOCKER', cast=bool):
    HOST = 'localhost'
    timeout = 50
else:
    HOST = 'db'
    timeout = 30000

db = MongoClient(HOST, 27017, connect=False, serverSelectionTimeoutMS=timeout).topkook
