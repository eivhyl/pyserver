"""
The flask application package.
"""
from flask import Flask

from decouple import config
import uuid

app = Flask(__name__)
app.config.update(
    PORT=config('PORT', cast=int),
    DEBUG=config('DEBUG', cast=bool),
    SECRET_KEY=config('SECRET_KEY', default=str(uuid.uuid4()))
)


def enumfilter(data):
    try:
        return data[5:]
    except:
        return 'qty'

app.jinja_env.filters['enum'] = enumfilter

from PyServer.views import *
