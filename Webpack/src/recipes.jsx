﻿//TODO: Add bootstrap thumbnail class
var Pages = React.createClass({
	getInitialState: function () {
		return { items: [] };
	},

	componentWillReceiveProps: function (nextProps) {
		var nextItems = this.state.items;
		var diff = nextProps.pages - this.props.pages;

		if (diff > 0)
			for (let i = 0; i <= diff; i++) 
				nextItems.push({ btnState: '' });

		else if (diff < 0)
			for (let i = diff; i < 0; i++)
				nextItems.pop();

		nextItems[this.props.currentPage - 1].btnState = '';
		nextItems[nextProps.currentPage - 1].btnState = 'active';

		this.setState({
			items: nextItems
		});
	},

	render: function () {
		var _this = this;
		return (
			<ul className="pagination">
				<li onClick={_this.props.onSwapPage.bind(null, _this.props.currentPage - 1)}>
					<a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>

				{this.state.items.map(function (props, i) {
					return (
						<li onClick={_this.props.onSwapPage.bind(null, i + 1)} className={props.btnState} key={i}>
							<a href="#">{i+1}</a>
						</li>
					);
				})}

				<li onClick={_this.props.onSwapPage.bind(null, _this.props.currentPage + 1)}>
					<a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
		);
	}
});
var RecipeList = React.createClass({
	render: function () {
		var createItem = function (recipe) {
			var image;
			if (recipe.image)
				image = <img src={recipe.image} />;
			else
				image = <img src="../static/images/404.png" />;

			return (
				<div key={recipe._id} className="panel panel-box col-xs-1">
					<a href={recipe._id}></a>
					<div className="panel-title">
						{image}
					</div>
					<div className="panel-body">
						<h3>{recipe.name}</h3>
					</div>
				</div>
			);
		};
		return <div className="row">{this.props.items.map(createItem)}</div>;
	}
});
var RecipeApp = React.createClass({
	getInitialState: function () {
		return { items: [], elemsPerPage: 10, pages: 1, currentPage: 1 };
	},
	loadPage: function(i) {
	    var _this = this;
        $.getJSON(
			'http://localhost:3000/recipes/search' +
				'?page=' + i +
				'&limit=' + this.state.elemsPerPage,

			function (data) {
				var nextItems = data['results'];
				_this.setState({
				    items: nextItems,
				    currentPage: i,
				    pages: Math.ceil(data['totalResults'] / _this.state.elemsPerPage)
				});
			}
		);
    },
	componentDidMount: function () {
	    this.loadPage(this.state.currentPage);
	},
	swapPage: function (i) {
        // Only swap to legal pages
		if (i !== this.state.currentPage && (i > 0 && i <= this.state.pages)) {
		    this.loadPage(i);
		}
	},

	render: function () {
		return (
			<div>
				<RecipeList items={this.state.items} />
				<Pages pages={this.state.pages} onSwapPage={this.swapPage} currentPage={this.state.currentPage} />
			</div>
		);
	}
});

ReactDOM.render(
	<RecipeApp />,
	document.getElementById('recipes')
);
